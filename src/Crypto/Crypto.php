<?php

namespace Hermes\Cerberus\Crypto;

/**
 * Interface Crypto
 * @package Hermes\Cerberus\CredentialManager\Crypto
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Crypto
{
    /**
     * @param string $rawValue
     * @param string $key
     *
     * @return string
     */
    public function encrypt(string $rawValue, string $key): string;

    /**
     * @param string $encryptedValue
     * @param string $key
     *
     * @return string
     */
    public function decrypt(string $encryptedValue, string $key): string;
}