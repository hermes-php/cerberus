<?php

namespace Hermes\Cerberus\Crypto;

/**
 * Class CryptoException
 * @package Hermes\Cerberus\CredentialManager\Crypto
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class CryptoException extends \RuntimeException
{
    /**
     * @return CryptoException
     */
    public static function notEnoughEntropy(): CryptoException
    {
        return new self('Encryption failed: Not enough entropy');
    }

    /**
     * @return CryptoException
     */
    public static function decodingFailed(): CryptoException
    {
        return new self('Decryption failed: could not decode');
    }

    /**
     * @return CryptoException
     */
    public static function truncatedMessage(): CryptoException
    {
        return new self('Decryption failed: message was truncated');
    }

    /**
     * @return CryptoException
     */
    public static function tamperedMessage(): CryptoException
    {
        return new self('Decryption failed: the message was tampered in transit');
    }

    /**
     * @param int $required
     *
     * @return CryptoException
     */
    public static function keyIsNotRequiredBytes(int $required): CryptoException
    {
        return new self(sprintf('Encryption failed: the key must be %s bytes long', $required));
    }

}