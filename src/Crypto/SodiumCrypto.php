<?php

namespace Hermes\Cerberus\Crypto;

/**
 * Class SodiumCrypto
 * @package Hermes\Cerberus\CredentialManager\Crypto
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class SodiumCrypto implements Crypto
{
    /**
     * @param string $rawValue
     * @param string $key
     *
     * @return string
     */
    public function encrypt(string $rawValue, string $key): string
    {
        $this->ensureKeyLength($key);

        try {
            $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        } catch (\Exception $e) {
            throw CryptoException::notEnoughEntropy();
        }

        $cipher = base64_encode(
            $nonce.
            sodium_crypto_secretbox(
                $rawValue,
                $nonce,
                $key
            )
        );
        sodium_memzero($rawValue);
        sodium_memzero($key);
        return $cipher;
    }

    /**
     * @param string $encryptedValue
     * @param string $key
     *
     * @return string
     */
    public function decrypt(string $encryptedValue, string $key): string
    {
        $this->ensureKeyLength($key);

        $decoded = base64_decode($encryptedValue);
        if ($decoded === false) {
            throw CryptoException::decodingFailed();
        }
        if (mb_strlen($decoded, '8bit') < (SODIUM_CRYPTO_SECRETBOX_NONCEBYTES + SODIUM_CRYPTO_SECRETBOX_MACBYTES)) {
            throw CryptoException::truncatedMessage();
        }

        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $cipherText = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $plain = sodium_crypto_secretbox_open(
            $cipherText,
            $nonce,
            $key
        );
        if ($plain === false) {
            throw CryptoException::tamperedMessage();
        }
        sodium_memzero($cipherText);
        sodium_memzero($key);
        return $plain;
    }

    /**
     * @param string $key
     */
    private function ensureKeyLength(string $key): void
    {
        if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_AUTH_KEYBYTES) {
            throw CryptoException::keyIsNotRequiredBytes(SODIUM_CRYPTO_AUTH_KEYBYTES);
        }
    }
}