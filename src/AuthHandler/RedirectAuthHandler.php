<?php

namespace Hermes\Cerberus\AuthHandler;

use Hermes\Cerberus\Exception\AuthenticationException;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * This Auth Handler redirects after authentication has been successful or fails.
 *
 * @package Hermes\Cerberus\AuthHandler
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class RedirectAuthHandler implements AuthHandler
{
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;
    /**
     * @var string
     */
    private $redirectOnSuccess;
    /**
     * @var string
     */
    private $redirectOnFailure;

    /**
     * RedirectAuthHandler constructor.
     *
     * @param ResponseFactoryInterface $responseFactory
     * @param string                   $redirectOnSuccess
     * @param string                   $redirectOnFailure
     */
    public function __construct(
        ResponseFactoryInterface $responseFactory,
        string $redirectOnSuccess = '/',
        string $redirectOnFailure = '/login'
    ) {
        $this->responseFactory = $responseFactory;
        $this->redirectOnSuccess = $redirectOnSuccess;
        $this->redirectOnFailure = $redirectOnFailure;
    }

    /**
     * @param Request                      $request
     * @param RequestHandler               $next
     * @param AuthenticationException|null $exception
     *
     * @return Response
     */
    public function handleFailure(Request $request, RequestHandler $next, AuthenticationException $exception = null): Response
    {
        $response = $this->responseFactory->createResponse(302);
        $response = $response->withHeader('Location', $this->redirectOnFailure);
        return $response;
    }

    /**
     * @param Request        $request
     * @param RequestHandler $next
     *
     * @return Response
     */
    public function handleSuccess(Request $request, RequestHandler $next): Response
    {
        $response = $this->responseFactory->createResponse(302);
        $response = $response->withHeader('Location', $this->redirectOnSuccess);
        return $response;
    }
}