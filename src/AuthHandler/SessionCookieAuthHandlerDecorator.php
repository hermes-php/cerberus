<?php

namespace Hermes\Cerberus\AuthHandler;

use HansOtt\PSR7Cookies\SetCookie;
use Hermes\Cerberus\AuthContext;
use Hermes\Cerberus\Crypto\Crypto;
use Hermes\Cerberus\Crypto\SodiumCrypto;
use Hermes\Cerberus\Exception\AuthenticationException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * This class decorates an AuthHandler to store a session cookie in
 * the response.
 *
 * @package Hermes\Cerberus\AuthHandler
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class SessionCookieAuthHandlerDecorator implements AuthHandler
{
    /**
     * @var AuthHandler
     */
    private $authHandler;
    /**
     * @var string
     */
    private $encryptionKey;
    /**
     * @var Crypto
     */
    private $crypto;
    /**
     * @var string
     */
    private $cookieName;
    /**
     * @var string|null
     */
    private $host;

    /**
     * SessionCookieAuthHandlerDecorator constructor.
     *
     * @param AuthHandler $authHandler
     * @param string      $encryptionKey
     * @param Crypto      $crypto
     * @param string      $cookieName
     * @param string|null $host
     */
    public function __construct(
        AuthHandler $authHandler,
        string $encryptionKey,
        Crypto $crypto = null,
        string $cookieName = 'session',
        string $host = null
    ) {
        $this->authHandler = $authHandler;
        $this->encryptionKey = $encryptionKey;
        $this->crypto = $crypto ?? new SodiumCrypto();
        $this->cookieName = $cookieName;
        $this->host = $host;
    }

    /**
     * @param Request                      $request
     * @param RequestHandler               $next
     * @param AuthenticationException|null $exception
     *
     * @return Response
     */
    public function handleFailure(Request $request, RequestHandler $next, AuthenticationException $exception = null): Response
    {
        return $this->authHandler->handleFailure($request, $next, $exception);
    }

    /**
     * @param Request        $request
     * @param RequestHandler $next
     *
     * @return Response
     * @throws \HansOtt\PSR7Cookies\InvalidArgumentException
     */
    public function handleSuccess(Request $request, RequestHandler $next): Response
    {
        $response = $this->authHandler->handleSuccess($request, $next);

        /** @var AuthContext $authContext */
        $authContext = $request->getAttribute(AuthContext::class);

        $setCookie = new SetCookie(
            $this->cookieName,
            $this->crypto->encrypt($authContext->getAccount()->getLogin(), $this->encryptionKey),
            0,
            '/',
            $this->host ?? $request->getUri()->getHost()
        );

        return $setCookie->addToResponse($response);
    }
}