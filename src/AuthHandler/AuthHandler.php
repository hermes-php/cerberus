<?php

namespace Hermes\Cerberus\AuthHandler;

use Hermes\Cerberus\Exception\AuthenticationException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * This service decides what to do with the Request after Authentication fails or
 * is successful.
 *
 * @package Hermes\Cerberus\AuthHandler
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface AuthHandler
{
    /**
     * This method handles Authentication failures. When this is called and
     * $exception is null, it means the user did't provide any credentials.
     *
     * Implementation MUST decide if will let an anonymous request to a secured
     * area continue. We provide an AnonymousHandlerDecorator to easily do this.
     *
     * When $exception is present, you SHOULD not let the request continue in a
     * normal way. This means password is wrong or that the Account is not in a valid
     * status.
     *
     * @param Request                      $request
     * @param RequestHandler               $next
     * @param AuthenticationException|null $exception
     *
     * @return Response
     */
    public function handleFailure(Request $request, RequestHandler $next, AuthenticationException $exception = null): Response;

    /**
     * This handles successful authentication. You can return a response to the
     * client, inject a cookie, store information in the database, and let the
     * request continue normally.
     *
     * If you need access to the Authenticated Account, you can obtain it from
     * the AuthContext::class attribute already present in the Request.
     *
     * @param Request        $request
     * @param RequestHandler $next
     *
     * @return Response
     */
    public function handleSuccess(Request $request, RequestHandler $next): Response;
}