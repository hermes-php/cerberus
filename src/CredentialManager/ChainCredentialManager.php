<?php

namespace Hermes\Cerberus\CredentialManager;

use Hermes\Cerberus\Account\Account;
use Psr\Http\Message\ServerRequestInterface;

/**
 * This credential manager allows the chained execution of different credential
 * managers.
 *
 * @package Hermes\Cerberus\CredentialManager
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class ChainCredentialManager implements CredentialManager
{
    /**
     * @var CredentialManager[]
     */
    private $credentialManagers;

    /**
     * ChainCredentialManager constructor.
     *
     * @param CredentialManager ...$credentialManagers
     */
    public function __construct(CredentialManager ...$credentialManagers)
    {
        $this->credentialManagers = $credentialManagers;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string|null
     */
    public function extractLogin(ServerRequestInterface $request): ?string
    {
        foreach ($this->credentialManagers as $credentialManager) {
            $result = $credentialManager->extractLogin($request);
            if (\is_string($result)) {
                return $result;
            }
        }
        return null;
    }

    /**
     * @param Account $account
     *
     * @return bool
     */
    public function credentialsAreValid(Account $account): bool
    {
        foreach ($this->credentialManagers as $credentialManager) {
            if ($credentialManager->credentialsAreValid($account)) {
                return true;
            }
        }
        return false;
    }
}