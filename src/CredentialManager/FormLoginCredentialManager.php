<?php

namespace Hermes\Cerberus\CredentialManager;

use Hermes\Cerberus\Account\Account;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class FormLoginCredentialManager
 * @package Hermes\Cerberus\CredentialManager
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FormLoginCredentialManager implements CredentialManager
{
    /**
     * @var string
     */
    private $loginRoute;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $loginField;
    /**
     * @var string
     */
    private $passwordField;
    /**
     * @var string|null
     */
    private $password;

    /**
     * FormLoginCredentialManager constructor.
     *
     * @param string $loginRoute
     * @param string $method
     * @param string $loginField
     * @param string $passwordField
     */
    public function __construct(
        string $loginRoute = '/login',
        string $method = 'POST',
        string $loginField = 'login',
        string $passwordField = 'password'
    ) {
        $this->loginRoute = $loginRoute;
        $this->method = $method;
        $this->loginField = $loginField;
        $this->passwordField = $passwordField;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string|null
     */
    public function extractLogin(ServerRequestInterface $request): ?string
    {
        if (
            $request->getMethod() !== $this->method
            || $request->getUri()->getPath() !== $this->loginRoute
        ) {
            return null;
        }

        $body = $request->getParsedBody();

        $login = $body[$this->loginField] ?? null;
        $password = $body[$this->passwordField] ?? null;

        if ($body === null || $login === null || $password === null) {
            return null;
        }
        $this->password = $password;

        return $login;
    }

    /**
     * @param Account $account
     *
     * @return bool
     */
    public function credentialsAreValid(Account $account): bool
    {
        $result = $account->isPasswordValid($this->password);
        $this->password = null;
        return $result;
    }
}