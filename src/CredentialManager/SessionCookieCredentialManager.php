<?php

namespace Hermes\Cerberus\CredentialManager;

use HansOtt\PSR7Cookies\RequestCookies;
use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\Crypto\Crypto;
use Hermes\Cerberus\Crypto\SodiumCrypto;
use Psr\Http\Message\ServerRequestInterface;

/**
 * This Credential Manager extracts credentials from a cookie.
 *
 * It is intended to use in sessions.
 *
 * MUST be used along with the SessionCookieAuthHandlerDecorator
 *
 * @see \Hermes\Cerberus\AuthHandler\SessionCookieAuthHandlerDecorator;
 *
 * @package Hermes\Cerberus\CredentialManager
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class SessionCookieCredentialManager implements CredentialManager
{
    /**
     * @var string
     */
    private $encryptionKey;
    /**
     * @var Crypto
     */
    private $crypto;
    /**
     * @var string
     */
    private $cookieName;

    /**
     * SessionCookieCredentialManager constructor.
     *
     * @param string      $encryptionKey
     * @param Crypto|null $crypto
     * @param string      $cookieName
     */
    public function __construct(string $encryptionKey, Crypto $crypto = null, string $cookieName = 'session')
    {
        $this->encryptionKey = $encryptionKey;
        $this->crypto = $crypto ?? new SodiumCrypto();
        $this->cookieName = $cookieName;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return string|null
     * @throws \HansOtt\PSR7Cookies\CookieNotFound
     */
    public function extractLogin(ServerRequestInterface $request): ?string
    {
        $cookies = RequestCookies::createFromRequest($request);
        if (!$cookies->has($this->cookieName)) {
            return null;
        }
        $cookie = $cookies->get($this->cookieName);
        return $this->crypto->decrypt($cookie->getValue(), $this->encryptionKey);
    }

    /**
     * @param Account $account
     *
     * @return bool
     */
    public function credentialsAreValid(Account $account): bool
    {
        return true;
    }
}