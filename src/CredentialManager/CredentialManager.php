<?php

namespace Hermes\Cerberus\CredentialManager;

use Hermes\Cerberus\Account\Account;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface CredentialManager
 * @package Hermes\Cerberus\CredentialManager
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface CredentialManager
{
    /**
     * This method extracts the login credentials from the request.
     *
     * MUST return a string that will be passed to the AccountProvider::loadAccount()
     * method to fetch/find the account.
     *
     * If the credentials involve a password check, you MUST store the password
     * in the implementation's memory, to be later used with
     * CredentialManager::credentialsAreValid() method.
     *
     * If you return null from here, authentication WILL fail because credentials
     * could not be extracted.
     *
     * @param ServerRequestInterface $request
     *
     * @return string|null
     */
    public function extractLogin(ServerRequestInterface $request): ? string;

    /**
     * Checks whether credentials from an account are valid or not.
     *
     * Implementations MUST clean the state of the save password if they are checking
     * that a password exists.
     *
     * @param Account $account
     *
     * @return bool
     */
    public function credentialsAreValid(Account $account): bool;
}