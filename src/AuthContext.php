<?php

namespace Hermes\Cerberus;

use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\Account\PrivilegeAwareAccount;

/**
 * Class AuthContext
 * @package Hermes\Cerberus
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class AuthContext
{
    /**
     * @var Account
     */
    private $account;

    /**
     * AuthContext constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account = null)
    {
        $this->account = $account;
    }

    /**
     * @return bool
     */
    public function isAnonymous(): bool
    {
        return $this->account === null;
    }

    /**
     * @return Account|null
     */
    public function getAccount(): ? Account
    {
        return $this->account;
    }

    /**
     * @param string $policy
     *
     * @return bool
     */
    public function can(string $policy): bool
    {
        if ($this->account === null) {
            return false;
        }
        if ($this->account instanceof PrivilegeAwareAccount) {
            return $this->account->can($policy);
        }
        return true;
    }

    /**
     * @param string $policy
     *
     * @return bool
     */
    public function cannot(string $policy): bool
    {
        if ($this->account === null) {
            return true;
        }
        if ($this->account instanceof PrivilegeAwareAccount) {
            return $this->account->cannot($policy);
        }
        return false;
    }
}