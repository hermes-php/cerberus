<?php

namespace Hermes\Cerberus\AccountProvider;

use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\Exception\AccountNotFoundException;

/**
 * Basic contract for an Account Provider.
 *
 * An account provider is a service that fetches a user account using a login, that
 * can be any string value, like an email, username or id.
 *
 * @package Hermes\Cerberus\AccountProvider
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface AccountProvider
{
    /**
     * @param string $loginOrId
     *
     * @throws AccountNotFoundException if account is not found.
     *
     * @return Account
     */
    public function loadAccount(string $loginOrId): Account;
}