<?php

namespace Hermes\Cerberus\AccountProvider;

use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\Exception\AccountNotFoundException;

/**
 * Class ChainAccountProvider
 * @package Hermes\Cerberus\AccountProvider
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class ChainAccountProvider implements AccountProvider
{
    /**
     * @var AccountProvider[]
     */
    private $providers;

    /**
     * ChainAccountProvider constructor.
     *
     * @param AccountProvider ...$providers
     */
    public function __construct(AccountProvider ...$providers)
    {
        $this->providers = $providers;
    }

    /**
     * @param string $login
     *
     * @return Account
     * @throws AccountNotFoundException
     */
    public function loadAccount(string $login): Account
    {
        foreach ($this->providers as $provider) {
            try {
                return $provider->loadAccount($login);
            } catch (AccountNotFoundException $exception) {
                continue;
            }
        }
        throw new AccountNotFoundException($login);
    }
}