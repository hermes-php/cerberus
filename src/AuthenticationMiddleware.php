<?php

namespace Hermes\Cerberus;

use Hermes\Cerberus\Firewall\Firewall;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Handler;
use Tuupola\Middleware\DoublePassTrait;

/**
 * Class AuthenticationMiddleware
 * @package Hermes\Cerberus\Http\Middleware
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    use DoublePassTrait;

    /**
     * @param Request $request
     * @param Handler $next
     *
     * @return Response
     */
    public function process(Request $request, Handler $next): Response
    {
        /** @var Firewall $firewall */
        $firewall = $request->getAttribute(Firewall::class);

        if (null === $firewall) {
            return $next->handle($request);
        }

        return $firewall->process($request, $next);
    }
}