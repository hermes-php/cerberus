<?php

namespace Hermes\Cerberus;

use Hermes\Cerberus\Firewall\Firewall;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Handler;

/**
 * Class FirewallMatcherMiddleware
 * @package Hermes\Cerberus
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class FirewallMatcherMiddleware implements MiddlewareInterface
{
    /**
     * @var Firewall[]
     */
    private $firewalls;

    /**
     * FirewallMatcherMiddleware constructor.
     *
     * @param Firewall ...$firewalls
     */
    public function __construct(Firewall ...$firewalls)
    {
        $this->firewalls = $firewalls;
    }

    /**
     * @param Request $request
     * @param Handler $next
     *
     * @return Response
     */
    public function process(Request $request, Handler $next): Response
    {
        foreach ($this->firewalls as $firewall) {
            if ($firewall->matches($request->getUri())) {
                $request = $request->withAttribute(Firewall::class, $firewall);
                break;
            }
        }

        return $next->handle($request);
    }
}