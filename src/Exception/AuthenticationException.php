<?php

namespace Hermes\Cerberus\Exception;

/**
 * Class AuthenticationException
 * @package Hermes\Cerberus\Authenticator
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AuthenticationException extends \RuntimeException
{

}