<?php

namespace Hermes\Cerberus\Exception;

/**
 * Class AccountStatusException
 * @package Hermes\Cerberus\Authenticator
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AccountStatusException extends AuthenticationException
{

}