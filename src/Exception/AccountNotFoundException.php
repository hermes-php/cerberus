<?php

namespace Hermes\Cerberus\Exception;


/**
 * Class AccountNotFoundException
 * @package Hermes\Cerberus\AccountProvider
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AccountNotFoundException extends AuthenticationException
{
    /**
     * AccountNotFoundException constructor.
     *
     * @param string $login
     */
    public function __construct(string $login)
    {
        parent::__construct(sprintf('Account "%s" not found', $login));
    }
}