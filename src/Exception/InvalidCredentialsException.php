<?php

namespace Hermes\Cerberus\Exception;

/**
 * Class InvalidCredentialsException
 * @package Hermes\Cerberus\Authenticator
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class InvalidCredentialsException extends AuthenticationException
{
    public function __construct()
    {
        parent::__construct('The provided credentials are invalid');
    }
}