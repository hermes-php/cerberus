<?php

namespace Hermes\Cerberus\Account;

/**
 * Interface HashedPassword
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface HashedPassword
{
    /**
     * Creates a new hashed password from a plain password.
     * @param string $plainPassword
     *
     * @return mixed
     */
    public static function fromPlainPassword(string $plainPassword): HashedPassword;

    /**
     * @param string $hash
     *
     * @return HashedPassword
     */
    public static function fromHash(string $hash): HashedPassword;

    /**
     * Checks whether a password is valid.
     * @param string $plainPassword
     *
     * @return bool
     */
    public function isValid(string $plainPassword): bool;
}