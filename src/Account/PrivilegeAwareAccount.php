<?php

namespace Hermes\Cerberus\Account;

/**
 * This interface provides a contract for adding user privileges to your account.
 *
 * It's main purpose is to check if the security context object should be populated
 * with the account privileges, so you can ask for them in your application.
 *
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface PrivilegeAwareAccount extends Account
{
    /**
     * @param string $policy
     *
     * @return bool
     */
    public function can(string $policy): bool;

    /**
     * @param string $policy
     *
     * @return bool
     */
    public function cannot(string $policy): bool;
}