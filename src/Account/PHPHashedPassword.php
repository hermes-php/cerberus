<?php

namespace Hermes\Cerberus\Account;

/**
 * Class PHPHashedPassword
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
abstract class PHPHashedPassword implements HashedPassword
{
    /**
     * @var string
     */
    private $hash;

    /**
     * @param string $plainPassword
     *
     * @return HashedPassword
     */
    public static function fromPlainPassword(string $plainPassword): HashedPassword
    {
        $self = new static();
        $self->hash = $self->hash($plainPassword);
        return $self;
    }

    /**
     * @param string $hash
     *
     * @return HashedPassword
     */
    public static function fromHash(string $hash): HashedPassword
    {
        $self = new static();
        $self->hash = $hash;
        return $self;
    }

    /**
     * @param string $plainPassword
     *
     * @return bool
     */
    public function isValid(string $plainPassword): bool
    {
        return $this->verify($plainPassword);
    }

    /**
     * @param string $plainPassword
     *
     * @return string
     */
    protected function hash(string $plainPassword): string
    {
        return password_hash($plainPassword, $this->getAlgo(), [
            'cost' => $this->getCost(),
        ]);
    }

    /**
     * @param string $plainPassword
     *
     * @return bool
     */
    protected function verify(string $plainPassword): bool
    {
        return password_verify($plainPassword, $this->hash);
    }

    /**
     * @return int
     */
    abstract protected function getAlgo(): int;

    /**
     * @return int
     */
    abstract protected function getCost(): int;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->hash;
    }
}