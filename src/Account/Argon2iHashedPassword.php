<?php

namespace Hermes\Cerberus\Account;

/**
 * Class Argon2iHashedPassword
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class Argon2iHashedPassword extends PHPHashedPassword
{
    /**
     * @return int
     */
    protected function getAlgo(): int
    {
        return PASSWORD_ARGON2I;
    }

    /**
     * @return int
     */
    protected function getCost(): int
    {
        return 16;
    }
}