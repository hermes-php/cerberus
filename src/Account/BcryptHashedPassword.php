<?php

namespace Hermes\Cerberus\Account;

/**
 * Class BcryptHashedPassword
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class BcryptHashedPassword extends PHPHashedPassword
{
    /**
     * @return int
     */
    protected function getAlgo(): int
    {
        return PASSWORD_BCRYPT;
    }

    /**
     * @return int
     */
    protected function getCost(): int
    {
        return 12;
    }
}