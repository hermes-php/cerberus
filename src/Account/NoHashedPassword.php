<?php

namespace Hermes\Cerberus\Account;

/**
 * Class NoHashedPassword
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class NoHashedPassword implements HashedPassword
{
    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @param string $plainPassword
     *
     * @return HashedPassword
     */
    public static function fromPlainPassword(string $plainPassword): HashedPassword
    {
        $self = new self();
        $self->plainPassword = $plainPassword;
        return $self;
    }

    /**
     * @param string $hash
     *
     * @return HashedPassword
     */
    public static function fromHash(string $hash): HashedPassword
    {
        return self::fromPlainPassword($hash);
    }

    /**
     * @param string $plainPassword
     *
     * @return bool
     */
    public function isValid(string $plainPassword): bool
    {
        return $this->plainPassword === $plainPassword;
    }
}