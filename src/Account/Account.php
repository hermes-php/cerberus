<?php

namespace Hermes\Cerberus\Account;

/**
 * This interface defines the basic contract for an account.
 *
 * It has the most basic elements of every account: a login string and a password
 * verification.
 *
 * This library provides the HashedPassword interface that you COULD use in your
 * model. It is really useful since it hides the implementation details of password
 * hashing process.
 *
 * @see HashedPassword
 * @see Argon2iHashedPassword
 * @see BcryptHashedPassword
 * @see NoHashedPassword
 *
 * You can extend the behaviour of your account implementation with a couple of
 * interfaces that check for privileges and for account status.
 *
 * @see PrivilegeAwareAccount
 * @see StatusAwareAccount
 *
 * Usually, the way of implementing this is by wrapping your own account or user
 * class in an implementation of this interface, and provide the required methods
 * directly from the object of your domain. Check the following link for an example.
 *
 * @link https://github.com/mnavarrocarter/auth-middleware/tree/master/docs/implementing-account-interface.md
 *
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface Account
{
    /**
     * @return string
     */
    public function getLogin(): string;

    /**
     * @param string $plainPassword
     *
     * @return bool
     */
    public function isPasswordValid(string $plainPassword): bool;
}