<?php

namespace Hermes\Cerberus\Account;

/**
 * Defines common privileges constants.
 *
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class Privilege
{
    public const DO_USER_TASKS = 'DO_USER_TASKS';
    public const DO_ADMIN_TASKS = 'DO_ADMIN_TASKS';
    public const IMPERSONATE = 'IMPERSONATE';

    /**
     * @return array
     */
    public function getValues(): array
    {
        return [
            self::DO_USER_TASKS,
            self::DO_ADMIN_TASKS,
            self::IMPERSONATE,
        ];
    }
}