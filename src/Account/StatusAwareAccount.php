<?php

namespace Hermes\Cerberus\Account;

/**
 * Interface StatusAwareAccount
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface StatusAwareAccount extends Account
{
    /**
     * Whether this account is in a ok status.
     *
     * @return bool
     */
    public function isStatusOk(): bool;

    /**
     * Provides the reason why this account is not in a ok status.
     * This will likely be seen by the end user.
     *
     * @return null|string
     */
    public function whyNot(): ?string;
}