<?php

namespace Hermes\Cerberus\Account;

/**
 * Class Privileges
 * @package Hermes\Cerberus\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
final class Privileges
{
    /**
     * The defined privileges
     * @var array
     */
    private static $privilegesMap = [
        Privilege::DO_USER_TASKS => 1,
        Privilege::DO_ADMIN_TASKS => 2,
        Privilege::IMPERSONATE => 4
    ];

    /**
     * The max number of privileges one can have.
     * @var int
     */
    private static $all = 7;

    /**
     * Whether the privileges have been defined already.
     * @var bool
     */
    private static $defined = false;

    /**
     * @var int
     */
    private $value;

    /**
     * @param string ...$names
     */
    public static function define(string ...$names): void
    {
        if (self::$defined === true) {
            throw new \RuntimeException('Privileges can be defined only once at Runtime');
        }
        self::$privilegesMap = [];
        $sum = 0;
        $number = null;
        foreach ($names as $name) {
            $number += $number ?? 1;
            $sum += $number;
            if (array_key_exists($name, self::$privilegesMap)) {
                throw new \RuntimeException(
                    sprintf('Duplicated key "%s" detected in privileges definition', $name)
                );
            }
            self::$privilegesMap[$name] = $number;
        }
        self::$all = $sum;
        self::$defined = true;
    }

    public static function all(): Privileges
    {
        return new self(self::$all);
    }

    /**
     * @param string ...$names
     *
     * @return Privileges
     */
    public static function fromName(string ...$names): Privileges
    {
        $self = new self(0);
        return $self->withGranted(...$names);
    }

    /**
     * Privileges constructor.
     *
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function value(): int
    {
        return $this->value;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function can(string $name): bool
    {
        $this->ensurePrivilegeExists($name);
        return $this->value & self::$privilegesMap[$name];
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function cannot(string $name): bool
    {
        return !$this->can($name);
    }

    /**
     * @param string[] $names
     *
     * @return Privileges
     */
    public function withGranted(string ...$names): Privileges
    {
        $clone = clone $this;
        foreach ($names as $name) {
            if ($clone->cannot($name)) {
                $clone->value += self::$privilegesMap[$name];
            }
        }
        return $clone;
    }

    /**
     * @param string[] $names
     *
     * @return Privileges
     */
    public function withRevoked(string ...$names): Privileges
    {
        $clone = clone $this;
        foreach ($names as $name) {
            if ($clone->can($name)) {
                $clone->value -= self::$privilegesMap[$name];
            }
        }
        return $clone;
    }

    /**
     * @param string $name
     */
    private function ensurePrivilegeExists(string $name): void
    {
        if (!array_key_exists($name, self::$privilegesMap)) {
            throw new \RuntimeException(
                sprintf('Privilege "%s" is not defined', $name)
            );
        }
    }
}