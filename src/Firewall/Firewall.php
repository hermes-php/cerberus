<?php

namespace Hermes\Cerberus\Firewall;

use Hermes\Cerberus\Account\StatusAwareAccount;
use Hermes\Cerberus\AccountProvider\AccountProvider;
use Hermes\Cerberus\AuthContext;
use Hermes\Cerberus\AuthHandler\AuthHandler;
use Hermes\Cerberus\CredentialManager\CredentialManager;
use Hermes\Cerberus\Exception\AccountStatusException;
use Hermes\Cerberus\Exception\AuthenticationException;
use Hermes\Cerberus\Exception\InvalidCredentialsException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as Handler;

/**
 * Class Firewall
 * @package Hermes\Cerberus\Firewall
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class Firewall implements MiddlewareInterface
{
    /**
     * @var string
     */
    private $pattern;
    /**
     * @var CredentialManager
     */
    private $credentialManager;
    /**
     * @var AccountProvider
     */
    private $accountProvider;
    /**
     * @var AuthHandler
     */
    private $handler;

    /**
     * Firewall constructor.
     *
     * @param string            $pattern
     * @param CredentialManager $credentialManager
     * @param AccountProvider   $accountProvider
     * @param AuthHandler       $handler
     */
    public function __construct(
        string $pattern,
        CredentialManager $credentialManager,
        AccountProvider $accountProvider,
        AuthHandler $handler
    ) {
        $this->pattern = $pattern;
        $this->credentialManager = $credentialManager;
        $this->accountProvider = $accountProvider;
        $this->handler = $handler;
    }

    /**
     * @param UriInterface $uri
     *
     * @return bool
     */
    public function matches(UriInterface $uri): bool
    {
        return 0 !== preg_match('/'.str_replace('/', '\/', $this->pattern).'/', $uri->getPath());
    }

    /**
     * @param Request $request
     * @param Handler $next
     *
     * @return Response
     */
    public function process(Request $request, Handler $next): Response
    {
        $login = $this->credentialManager->extractLogin($request);

        if (null === $login) {
            return $this->handler->handleFailure($request, $next);
        }

        try {
            $account = $this->accountProvider->loadAccount($login);

            if (!$this->credentialManager->credentialsAreValid($account)) {
                throw new InvalidCredentialsException();
            }
            if ($account instanceof StatusAwareAccount && ! $account->isStatusOk()) {
                throw new AccountStatusException($account->whyNot());
            }

            $authContext = new AuthContext($account);

            $request = $request->withAttribute(AuthContext::class, $authContext);

            return $this->handler->handleSuccess($request, $next);

        } catch (AuthenticationException $exception) {

            return $this->handler->handleFailure($request, $next, $exception);

        }
    }
}