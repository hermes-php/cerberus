Cerberus
========

A full-blown PSR-7 authentication system inspired in Symfony Security Component.

The main purpose of this middleware is to authenticate the request using flexible
and user defined rules and generate an AuthenticationContext object to pass to the
rest of the middleware stack.

Firewall: Must be configured with the following things:
- pattern
- CredentialManager (two methods: extract and verify)
- AccountProvider
- AuthHandler (two methods: handle success, handle error)

Firewall it's a middleware in itself, so we have three middleware:
- Firewall detection middleware: checks if a middleware matches and stores it
in a request property.
- Firewall execution middleware: fetches the middleware from the request, and
applies it. If auth is successful, then request will have a AuthContext object.

Implementations: 
- LazyCredentialManager
- LazyAuthHandler
- LazyAccountProvider
- ChainAccountProvider
- LazyFirewall
- JwtCredentialManager
- FormLoginCredentialManager
- CookieCredentialManager
- ChainCredentialManager
- RedirectAuthHandler
- CookieRedirectAuthHandler
- JsonResponseAuthHandler

3 steps form:
- extract credentials
- extract user
- verify password
