<?php

namespace Hermes\Cerberus\Tests;

use Hermes\Cerberus\Firewall\Firewall;
use Hermes\Cerberus\FirewallMatcherMiddleware;

/**
 * Class FirewallMatcherMiddlewareTest
 * @package Hermes\Cerberus\Tests
 */
class FirewallMatcherMiddlewareTest extends AuthenticationTestCase
{
    public function testFirewallMatches(): void
    {
        $requestHandler = $this->createRequestHandler();
        $requestHandler->expects($this->once())
            ->method('handle');

        $credentialManager = $this->createCredentialManager();
        $accountProvider = $this->createAccountProvider();

        $authHandler = $this->createAuthHandler();

        $firewallOne = new Firewall('^/', $credentialManager, $accountProvider, $authHandler);
        $firewallTwo = new Firewall('^/admin', $credentialManager, $accountProvider, $authHandler);

        $request = $this->createRequest('GET', '/users');
        $request->expects($this->once())
            ->method('withAttribute')
            ->with(Firewall::class, $firewallOne)
            ->willReturn($request);

        $middleware = new FirewallMatcherMiddleware($firewallOne, $firewallTwo);

        $middleware->process($request, $requestHandler);
    }

    public function testSecondFirewallMatches(): void
    {
        $requestHandler = $this->createRequestHandler();
        $requestHandler->expects($this->once())
            ->method('handle');

        $credentialManager = $this->createCredentialManager();
        $accountProvider = $this->createAccountProvider();

        $authHandler = $this->createAuthHandler();

        $firewallOne = new Firewall('^/', $credentialManager, $accountProvider, $authHandler);
        $firewallTwo = new Firewall('^/admin', $credentialManager, $accountProvider, $authHandler);

        $request = $this->createRequest('GET', '/admin/users');
        $request->expects($this->once())
            ->method('withAttribute')
            ->with(Firewall::class, $firewallOne)
            ->willReturn($request);

        $middleware = new FirewallMatcherMiddleware($firewallOne, $firewallTwo);

        $middleware->process($request, $requestHandler);
    }

    public function testNoFirewallMatches(): void
    {
        $requestHandler = $this->createRequestHandler();
        $requestHandler->expects($this->once())
            ->method('handle');

        $credentialManager = $this->createCredentialManager();
        $accountProvider = $this->createAccountProvider();

        $authHandler = $this->createAuthHandler();

        $firewallOne = new Firewall('^/auth', $credentialManager, $accountProvider, $authHandler);
        $firewallTwo = new Firewall('^/admin', $credentialManager, $accountProvider, $authHandler);

        $request = $this->createRequest('GET', '/hello');

        $middleware = new FirewallMatcherMiddleware($firewallOne, $firewallTwo);

        $middleware->process($request, $requestHandler);
    }
}
