<?php

namespace Hermes\Cerberus\Tests\Crypto;

use Hermes\Cerberus\Crypto\CryptoException;
use Hermes\Cerberus\Crypto\SodiumCrypto;
use PHPUnit\Framework\TestCase;

class SodiumCryptoTest extends TestCase
{
    public function testEncryption(): void
    {
        $string = 'username';
        $key = random_bytes(SODIUM_CRYPTO_AUTH_KEYBYTES);
        $crypto = new SodiumCrypto();
        $encryptedString = $crypto->encrypt($string, $key);

        $this->assertEquals('username', $crypto->decrypt($encryptedString, $key));
    }

    public function testInvalidKey(): void
    {
        $string = 'username';
        $key = 'invalidkey';
        $crypto = new SodiumCrypto();
        $this->expectException(CryptoException::class);
        $encryptedString = $crypto->encrypt($string, $key);
    }

    public function testMessageWasTampered(): void
    {
        $string = 'username';
        $key = random_bytes(SODIUM_CRYPTO_AUTH_KEYBYTES);
        $crypto = new SodiumCrypto();
        $encryptedString = $crypto->encrypt($string, $key);

        $tamperedEncryptedString = str_replace(['b', '6', '+'], ['A', '3', '/'], $encryptedString);

        $this->expectException(CryptoException::class);
        $crypto->decrypt($tamperedEncryptedString, $key);
    }

    public function testTruncatedMessage(): void
    {
        $string = 'username';
        $key = random_bytes(SODIUM_CRYPTO_AUTH_KEYBYTES);
        $crypto = new SodiumCrypto();
        $encryptedString = $crypto->encrypt($string, $key);

        $tamperedEncryptedString = substr($encryptedString, 0, -3);

        $this->expectException(CryptoException::class);
        $crypto->decrypt($tamperedEncryptedString, $key);
    }
}
