<?php

namespace Hermes\Cerberus\Tests\Account;

use Hermes\Cerberus\Account\BcryptHashedPassword;
use PHPUnit\Framework\TestCase;

class BcryptHashedPasswordTest extends TestCase
{
    public function testPasswordVerification(): void
    {
        $password = BcryptHashedPassword::fromPlainPassword('somepassword');
        $this->assertTrue($password->isValid('somepassword'));
        $this->assertFalse($password->isValid('wrongPassword'));
    }
}
