<?php

namespace Hermes\Cerberus\Tests\Account;

use Hermes\Cerberus\Account\Argon2iHashedPassword;
use Hermes\Cerberus\Account\BcryptHashedPassword;
use PHPUnit\Framework\TestCase;

/**
 * Class Argon2iHashedPasswordTest
 * @package Hermes\Cerberus\Tests\Account
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class Argon2iHashedPasswordTest extends TestCase
{
    public function testPasswordVerification(): void
    {
        if (null === @\constant('PASSWORD_ARGON2I')) {
            $this->markTestIncomplete(sprintf('Could not complete %s because Argon2i is not compiled with this version of PHP.', __CLASS__));
            return;
        }

        $password = Argon2iHashedPassword::fromPlainPassword('somepassword');
        $this->assertTrue($password->isValid('somepassword'));
        $this->assertFalse($password->isValid('wrongPassword'));
    }
}
