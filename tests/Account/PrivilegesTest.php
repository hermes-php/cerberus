<?php

namespace Hermes\Cerberus\Tests\Account;

use Hermes\Cerberus\Account\Privilege;
use Hermes\Cerberus\Account\Privileges;
use PHPUnit\Framework\TestCase;

class PrivilegesTest extends TestCase
{
    public function testThatCanBeInitialized(): void
    {
        $privileges = Privileges::all();
        $this->assertTrue($privileges->can(Privilege::DO_USER_TASKS));
    }

    public function testThatUndefinedPrivilegeFails(): void
    {
        $privileges = Privileges::all();
        $this->expectException(\RuntimeException::class);
        $privileges->can('DO_SOMETHING_NOT_DEFINED');
    }

    public function testCustomPrivilegesCanBeDefined(): void
    {
        Privileges::define('do_something', 'do_another_thing', 'do_something_else');
        $privileges = Privileges::all();

        $this->assertEquals(7, $privileges->value());
        $this->assertTrue($privileges->can('do_something'));
        $this->assertTrue($privileges->can('do_another_thing'));
        $this->assertTrue($privileges->can('do_something_else'));
    }

    public function testRedefinitionFails(): void
    {
        $this->expectException(\RuntimeException::class);
        Privileges::define('do_something', 'do_another_thing', 'do_something_else');
    }

    public function testCreationFromNames(): void
    {
        $privileges = Privileges::fromName('do_something', 'do_another_thing');

        $this->assertTrue($privileges->can('do_something'));
        $this->assertTrue($privileges->can('do_another_thing'));
        $this->assertTrue($privileges->cannot('do_something_else'));

        $this->assertEquals(3, $privileges->value());
    }

    public function testPrivilegesCanChange(): void
    {
        $privileges = Privileges::all();
        $privileges = $privileges->withRevoked('do_something');

        $this->assertTrue($privileges->cannot('do_something'));
        $this->assertTrue($privileges->can('do_another_thing'));
        $this->assertTrue($privileges->can('do_something_else'));
    }
}
