<?php

namespace Hermes\Cerberus\Tests\CredentialManager;

use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\CredentialManager\FormLoginCredentialManager;
use Hermes\Cerberus\Tests\AuthenticationTestCase;
use Psr\Http\Message\ServerRequestInterface;

class FormLoginCredentialManagerTest extends AuthenticationTestCase
{
    public function testThatCredentialsAreExtracted(): void
    {
        $request = $this->createRequest('POST', '/login', [
            'login' => 'myusername',
            'password' => 'mypassword'
        ]);

        $account = $this->createMock(Account::class);
        $account->expects($this->once())
            ->method('isPasswordValid')
            ->with('mypassword')
            ->willReturn(true);

        $credentialManager = new FormLoginCredentialManager();
        $this->assertEquals('myusername', $credentialManager->extractLogin($request));

        $this->assertTrue($credentialManager->credentialsAreValid($account));
    }

    public function testThatBodyCannotBeParsed(): void
    {
        $request = $this->createRequest('POST', '/login', null);

        $credentialManager = new FormLoginCredentialManager();
        $this->assertNull($credentialManager->extractLogin($request));

    }

    public function testThatInvalidMethodIsPassed(): void
    {
        $request = $this->createRequest('GET', '/users', null);

        $credentialManager = new FormLoginCredentialManager();
        $this->assertNull($credentialManager->extractLogin($request));
    }
}
