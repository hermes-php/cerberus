<?php

namespace Hermes\Cerberus\Tests;

use Hermes\Cerberus\Account\PrivilegeAwareAccount;
use Hermes\Cerberus\AuthContext;

class AuthContextTest extends AuthenticationTestCase
{
    public function testGetAccount(): void
    {
        $account = $this->createAccount('fake', true);

        $authContext = new AuthContext($account);

        $this->assertEquals($account, $authContext->getAccount());
    }

    public function testCan(): void
    {
        $account = $this->createMock(PrivilegeAwareAccount::class);
        $account->expects($this->once())
            ->method('can')
            ->willReturn(true);

        $authContext = new AuthContext($account);
        $this->assertTrue($authContext->can('enter'));
    }

    public function testCannot(): void
    {
        $account = $this->createMock(PrivilegeAwareAccount::class);
        $account->expects($this->once())
            ->method('cannot')
            ->willReturn(false);

        $authContext = new AuthContext($account);
        $this->assertFalse($authContext->cannot('enter'));
    }

    public function testIsAnonymous(): void
    {
        $context = new AuthContext();

        $this->assertTrue($context->isAnonymous());
        $this->assertTrue($context->cannot('something'));
        $this->assertFalse($context->can('something'));
    }
}
