<?php

namespace Hermes\Cerberus\Tests\AuthHandler;

use Hermes\Cerberus\AuthContext;
use Hermes\Cerberus\AuthHandler\SessionCookieAuthHandlerDecorator;
use Hermes\Cerberus\Tests\AuthenticationTestCase;
use Psr\Http\Message\ResponseInterface;

class SessionCookieAuthHandlerDecoratorTest extends AuthenticationTestCase
{
    public function testThatCookieIsStored(): void
    {
        $account = $this->createAccount('username', true);
        $authContext = new AuthContext($account);

        $request = $this->createRequest('GET', '/hello');
        $request->expects($this->once())
            ->method('getAttribute')
            ->with(AuthContext::class)
            ->willReturn($authContext);

        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('withAddedHeader')
            ->with('Set-Cookie')
            ->willReturn($response);

        $requestHandler = $this->createRequestHandler();
        $mockedAuthHandler = $this->createAuthHandler();
        $mockedAuthHandler->expects($this->once())
            ->method('handleSuccess')
            ->willReturn($response);

        $key = random_bytes(SODIUM_CRYPTO_AUTH_KEYBYTES);


        $authHandler = new SessionCookieAuthHandlerDecorator($mockedAuthHandler, $key, null, 'session', 'localhost');

        $authHandler->handleSuccess($request, $requestHandler);
    }
}
