<?php

namespace Hermes\Cerberus\Tests;

use Hermes\Cerberus\Account\StatusAwareAccount;
use Hermes\Cerberus\AccountProvider\AccountProvider;
use Hermes\Cerberus\AuthHandler\AuthHandler;
use Hermes\Cerberus\CredentialManager\CredentialManager;
use Hermes\Cerberus\Firewall\Firewall;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class FirewallTest
 * @package Hermes\Cerberus\Tests
 */
class FirewallTest extends AuthenticationTestCase
{
    public function testThatExtractingLoginFails(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $requestHandler = $this->createMock(RequestHandlerInterface::class);

        $credentialManager = $this->createMock(CredentialManager::class);
        $credentialManager->expects($this->once())
            ->method('extractLogin')
            ->with($request)
            ->willReturn(null);

        $accountProvider = $this->createAccountProvider();

        $authHandler = $this->createMock(AuthHandler::class);
        $authHandler->expects($this->once())
            ->method('handleFailure')
            ->with($request, $requestHandler);

        $firewall = new Firewall(
            '^/',
            $credentialManager,
            $accountProvider,
            $authHandler
        );

        $firewall->process($request, $requestHandler);
    }

    public function testThatLoadingAccountFails(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $requestHandler = $this->createMock(RequestHandlerInterface::class);

        $credentialManager = $this->createMock(CredentialManager::class);
        $credentialManager->expects($this->once())
            ->method('extractLogin')
            ->with($request)
            ->willReturn('username');

        $accountProvider = $this->createAccountProvider();

        $authHandler = $this->createMock(AuthHandler::class);
        $authHandler->expects($this->once())
            ->method('handleFailure')
            ->with($request, $requestHandler);

        $firewall = new Firewall(
            '^/',
            $credentialManager,
            $accountProvider,
            $authHandler
        );

        $firewall->process($request, $requestHandler);
    }

    public function testThatCredentialsAreInvalid(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $requestHandler = $this->createMock(RequestHandlerInterface::class);

        $credentialManager = $this->createMock(CredentialManager::class);
        $credentialManager->expects($this->once())
            ->method('extractLogin')
            ->with($request)
            ->willReturn('username');

        $account = $this->createAccount('fake', false);

        $accountProvider = $this->createAccountProvider($account);

        $authHandler = $this->createMock(AuthHandler::class);
        $authHandler->expects($this->once())
            ->method('handleFailure')
            ->with($request, $requestHandler);

        $firewall = new Firewall(
            '^/',
            $credentialManager,
            $accountProvider,
            $authHandler
        );

        $firewall->process($request, $requestHandler);
    }

    public function testThatAccountStatusIsInvalid(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $requestHandler = $this->createMock(RequestHandlerInterface::class);

        $credentialManager = $this->createMock(CredentialManager::class);
        $credentialManager->expects($this->once())
            ->method('extractLogin')
            ->with($request)
            ->willReturn('username');
        $credentialManager->expects($this->once())
            ->method('credentialsAreValid')
            ->willReturn(true);

        $account = $this->createMock(StatusAwareAccount::class);

        $account->expects($this->once())
            ->method('isStatusOk')
            ->willReturn(false);
        $account->expects($this->once())
            ->method('whyNot');

        $accountProvider = $this->createMock(AccountProvider::class);
        $accountProvider->expects($this->once())
            ->method('loadAccount')
            ->willReturn($account);

        $authHandler = $this->createMock(AuthHandler::class);
        $authHandler->expects($this->once())
            ->method('handleFailure')
            ->with($request, $requestHandler);

        $firewall = new Firewall(
            '^/',
            $credentialManager,
            $accountProvider,
            $authHandler
        );

        $firewall->process($request, $requestHandler);
    }

    public function testThatLoginIsSuccessful(): void
    {
        $request = $this->createMock(ServerRequestInterface::class);
        $request->expects($this->once())
            ->method('withAttribute')
            ->willReturn($request);

        $requestHandler = $this->createMock(RequestHandlerInterface::class);

        $credentialManager = $this->createMock(CredentialManager::class);
        $credentialManager->expects($this->once())
            ->method('extractLogin')
            ->with($request)
            ->willReturn('username');
        $credentialManager->expects($this->once())
            ->method('credentialsAreValid')
            ->willReturn(true);

        $account = $this->createMock(StatusAwareAccount::class);

        $account->expects($this->once())
            ->method('isStatusOk')
            ->willReturn(true);

        $accountProvider = $this->createMock(AccountProvider::class);
        $accountProvider->expects($this->once())
            ->method('loadAccount')
            ->willReturn($account);

        $authHandler = $this->createMock(AuthHandler::class);
        $authHandler->expects($this->once())
            ->method('handleSuccess')
            ->with($request, $requestHandler);

        $firewall = new Firewall(
            '^/',
            $credentialManager,
            $accountProvider,
            $authHandler
        );

        $firewall->process($request, $requestHandler);
    }
}
