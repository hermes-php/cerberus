<?php

namespace Hermes\Cerberus\Tests;

use Hermes\Cerberus\AuthenticationMiddleware;
use Hermes\Cerberus\Firewall\Firewall;

class AuthenticationMiddlewareTest extends AuthenticationTestCase
{
    public function testFindingFirewall(): void
    {
        $firewall = $this->createMock(Firewall::class);
        $request = $this->createRequest('GET', '/hello');
        $handler = $this->createRequestHandler();

        $request->expects($this->once())
            ->method('getAttribute')
            ->with(Firewall::class)
            ->willReturn($firewall);
        $firewall->expects($this->once())
            ->method('process')
            ->with($request, $handler);

        $middleware = new AuthenticationMiddleware();

        $middleware->process($request, $handler);
    }

    public function testNotFindingMiddleware(): void
    {
        $request = $this->createRequest('GET', '/hello');
        $handler = $this->createRequestHandler();

        $request->expects($this->once())
            ->method('getAttribute')
            ->with(Firewall::class)
            ->willReturn(null);

        $handler->expects($this->once())
            ->method('handle')
            ->with($request);

        $middleware = new AuthenticationMiddleware();

        $middleware->process($request, $handler);
    }
}
