<?php

namespace Hermes\Cerberus\Tests;

use Hermes\Cerberus\Account\Account;
use Hermes\Cerberus\AccountProvider\AccountProvider;
use Hermes\Cerberus\AuthHandler\AuthHandler;
use Hermes\Cerberus\CredentialManager\CredentialManager;
use Hermes\Cerberus\Exception\AccountNotFoundException;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Trait AuthenticationTestCase
 * @package Hermes\Cerberus\Tests
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AuthenticationTestCase extends TestCase
{
    /**
     * @param string $method
     * @param string $path
     * @param array  $parsedBody
     *
     * @param array  $attributes
     *
     * @return \PHPUnit\Framework\MockObject\MockObject|ServerRequestInterface
     */
    protected function createRequest(string $method, string $path, ?array $parsedBody = [])
    {
        $uri = $this->createMock(UriInterface::class);
        $uri->method('getPath')->willReturn($path);

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($uri);
        $request->method('getParsedBody')->willReturn($parsedBody);

        return $request;
    }

    /**
     * @param Account|null $withAccount
     *
     * @return AccountProvider|\PHPUnit\Framework\MockObject\MockObject
     */
    public function createAccountProvider(Account $withAccount = null)
    {
        $provider = $this->createMock(AccountProvider::class);
        if ($withAccount === null) {
            $provider->method('loadAccount')
                ->willThrowException(new AccountNotFoundException('fake'));
        }
        $provider->method('loadAccount')->willReturn($withAccount);

        return $provider;
    }

    /**
     * @param string $login
     * @param bool   $isPasswordValid
     *
     * @return Account|\PHPUnit\Framework\MockObject\MockObject
     */
    public function createAccount(string $login, bool $isPasswordValid = true)
    {
        $account = $this->createMock(Account::class);
        $account->method('getLogin')->willReturn($login);
        $account->method('isPasswordValid')->willReturn($isPasswordValid);

        return $account;
    }

    /**
     * @param array $services
     *
     * @return \PHPUnit\Framework\MockObject\MockObject|ContainerInterface
     */
    public function createContainer(array $services)
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->method('get')->willReturnCallback(function ($serviceId) use ($services) {
            return $services[$serviceId];
        });
        $container->method('has')->willReturnCallback(function ($serviceId) use ($services) {
            return array_key_exists($serviceId, $services);
        });
        return $container;
    }

    /**
     * @param ResponseInterface $response
     *
     * @return \PHPUnit\Framework\MockObject\MockObject|ResponseFactoryInterface
     */
    public function createResponseFactory(ResponseInterface $response)
    {
        $factory = $this->createMock(ResponseFactoryInterface::class);
        $factory->method('createResponse')
            ->willReturn($response);

        return $factory;
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|RequestHandlerInterface
     */
    public function createRequestHandler()
    {
        return $this->createMock(RequestHandlerInterface::class);
    }

    /**
     * @return CredentialManager|\PHPUnit\Framework\MockObject\MockObject
     */
    public function createCredentialManager()
    {
        return $this->createMock(CredentialManager::class);
    }

    /**
     * @return AuthHandler|\PHPUnit\Framework\MockObject\MockObject
     */
    public function createAuthHandler()
    {
        return $this->createMock(AuthHandler::class);
    }
}